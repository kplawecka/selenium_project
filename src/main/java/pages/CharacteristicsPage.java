package pages;

import config.Config;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class CharacteristicsPage extends HomePage {

    private String pageUrl = new Config().getApplicationUrl()+"Characteristics";

    public CharacteristicsPage(WebDriver driver) {
        super(driver);
    }



    @FindBy(css = ".page-title h3")
    private WebElement pageHeader;

    public CharacteristicsPage assertCharacteristicsIsShown(){
        Assert.assertTrue(pageHeader.getText().contains("Characteristics"));

        return this;
    }

    public CharacteristicsPage assertCharacteristicsUrl(){
        Assert.assertEquals(driver.getCurrentUrl(), pageUrl);

        return this;
    }
}
