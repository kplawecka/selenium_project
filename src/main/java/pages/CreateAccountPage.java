package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import java.util.List;

public class CreateAccountPage extends SeleniumPage {

    public CreateAccountPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = "#Email")
    private WebElement nameTxt;

    @FindBy(css = "#Password")
    private WebElement passwordTxt;

    @FindBy(css = "#ConfirmPassword")
    private WebElement confirmPassTxt;

    @FindBy(css = "button[type=submit]")
    private WebElement registerBtn;

    @FindBy(css = ".login_content h1")
    private WebElement pageHeader;

    @FindBy(css = "#ConfirmPassword-error")
    private WebElement passError;

    @FindBy(css = ".validation-summary-errors li")
    private List<WebElement> errorFields;

    public CreateAccountPage typeEmail(String email) {
        nameTxt.clear();
        nameTxt.sendKeys(email);

        return this;
    }

    public CreateAccountPage typePassword(String pass) {
        passwordTxt.clear();
        passwordTxt.sendKeys(pass);

        return this;
    }

    public CreateAccountPage typeConfirmPass(String confirmPass) {
        confirmPassTxt.clear();
        confirmPassTxt.sendKeys(confirmPass);

        return this;
    }

    public HomePage clickRegisterBtn() {
        registerBtn.click();

        return new HomePage(driver);
    }

    public CreateAccountPage clickRegisterWithFailure() {
        registerBtn.click();

        return this;
    }

    public CreateAccountPage assertPageHeaderIsShown() {
        Assert.assertEquals(pageHeader.getText(), "Create Account");

        return this;
    }

    public CreateAccountPage assertPassConfirmErrorIsShown(String expPassError) {
        Assert.assertEquals(passError.getText(), expPassError);

        return this;
    }

    public CreateAccountPage assertRegistrationError(String expError) {
        Assert.assertEquals(errorFields.get(0).getText(), expError);

        return this;
    }


}
