package pages;

import config.Config;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;


public class CreateProcessPage extends HomePage {

    private String pageUrl = new Config().getApplicationUrl()+"Projects/Create";

    public CreateProcessPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = ".x_title")
    private WebElement pageHeader;

    @FindBy(css = "input[name='Name']")
    private WebElement nameTxt;

    @FindBy(css = "#Description")
    private WebElement descriptionTxt;

    @FindBy(css = "#Notes")
    private WebElement notesTxt;

    @FindBy(css = "input[value=Create")
    private WebElement createBtn;


    public CreateProcessPage typeProcessName(String processName){
        nameTxt.clear();
        nameTxt.sendKeys(processName);

        return this;
    }

    public CreateProcessPage typeDescription (String processDescription){
        descriptionTxt.clear();
        descriptionTxt.sendKeys(processDescription);

        return this;
    }

    public CreateProcessPage typeNotes (String processNotes){
        notesTxt.clear();
        notesTxt.sendKeys(processNotes);

        return this;
    }

    public ProcessesPage submitCreateProcess(){
        createBtn.click();

        return new ProcessesPage(driver);
    }

    public CreateProcessPage assertCreateProcessHeaderIsShown(){
        Assert.assertTrue(pageHeader.getText().contains("Create process"));

        return this;
    }

    public CreateProcessPage assertCreateProcessUrl(){
        Assert.assertEquals(driver.getCurrentUrl(), pageUrl);

        return this;
    }
}
