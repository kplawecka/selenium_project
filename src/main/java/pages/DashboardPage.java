package pages;

import config.Config;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class DashboardPage extends HomePage {

    private String pageUrl = new Config().getApplicationUrl();

    public DashboardPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = ".x_title h2")
    private WebElement pageHeader;

    public DashboardPage assertDashboardIsShown(){
        Assert.assertTrue(pageHeader.getText().contains("DEMO PROJECT"));

        return this;
    }

    public DashboardPage assertDashboardUrl(){
        Assert.assertEquals(driver.getCurrentUrl(), pageUrl);

        return this;
    }
}
