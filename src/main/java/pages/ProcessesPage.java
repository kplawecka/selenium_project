package pages;

import config.Config;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class ProcessesPage extends HomePage {

    private final String GENERIC_PROCESS_ROW_XPATH = "//td[text()='%s']/..";
    private String pageUrl = new Config().getApplicationUrl()+"Projects";

    public ProcessesPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = ".page-title h3")
    private WebElement pageHeader;

    @FindBy(css = "a[href$='Create']")
    private WebElement addNewProcessBtn;


    public CreateProcessPage clickAddNewProcess(){
        addNewProcessBtn.click();

        return new CreateProcessPage(driver);
    }

    public ProcessesPage assertProcessesIsShown(){
        Assert.assertTrue(pageHeader.getText().contains("Processes"));

        return this;
    }

    public ProcessesPage assertProcessessUrl(){
        Assert.assertEquals(driver.getCurrentUrl(), pageUrl);

        return this;

    }

    public ProcessesPage assertProcess(String expName, String expDescription, String expNotes){
        String processXpath = String.format(GENERIC_PROCESS_ROW_XPATH, expName);
        WebElement processRow = driver.findElement(By.xpath(processXpath));

        String actDescription = processRow.findElement(By.xpath("./td[2]")).getText();
        String actNotes = processRow.findElement(By.xpath("./td[3]")).getText();

        Assert.assertEquals(actDescription, expDescription);
        Assert.assertEquals(actNotes, expNotes);

        return this;
    }
}
