import org.testng.annotations.Test;
import pages.LoginPage;

import java.util.UUID;

public class Correct_Add_Process_Test extends SeleniumBaseTest {


    private String email = config.getApplicationUser();
    private String password = config.getApplicationPassword();
    private String name = UUID.randomUUID().toString().substring(0,10);
    private String description = "Equipment";
    private String notes = "Design, materials, components";


    @Test
    public void correctAddProcess(){
        new LoginPage(driver)
                .typeEmail(email)
                .typePassword(password)
                .submitLogin()
                .goToProcesses()
                    .assertProcessesIsShown()
                .clickAddNewProcess()
                    .assertCreateProcessHeaderIsShown()
                    .assertCreateProcessUrl()
                .typeProcessName(name)
                .typeDescription(description)
                .typeNotes(notes)
                .submitCreateProcess()
                    .assertProcess(name,description,notes);
    }
}
