import org.testng.annotations.Test;
import pages.LoginPage;

public class Correct_Login_Test extends SeleniumBaseTest {

    private String email = config.getApplicationUser();
    private String password = config.getApplicationPassword();

    @Test
    public void correctLogin(){
        new LoginPage(driver)
                .typeEmail(email)
                .typePassword(password)
                .submitLogin()
                    .assertLogoutBtnIsShown();
    }
}

