import org.testng.annotations.Test;
import pages.LoginPage;

public class Correct_Register_Test extends SeleniumBaseTest {

    private String email = faker.internet().emailAddress();
    private String password = "Test123.";
    private String confirmPass = password;

    //    private String email = UUID.randomUUID().toString().substring(0,10)+"@test.com";

    @Test
    public void correctRegister(){
        new LoginPage(driver)
                .clickRegisterLnk()
                    .assertPageHeaderIsShown()
                .typeEmail(email)
                .typePassword(password)
                .typeConfirmPass(confirmPass)
                .clickRegisterBtn()
                    .assertLogoutBtnIsShown();
    }
}
