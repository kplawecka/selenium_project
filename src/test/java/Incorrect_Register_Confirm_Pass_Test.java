import org.testng.annotations.Test;
import pages.LoginPage;

public class Incorrect_Register_Confirm_Pass_Test extends SeleniumBaseTest {

    private String email = faker.internet().emailAddress();
    private String pass = "Test123.";
    private String confirmPass = "Test12.";
    private String expPassError = "The password and confirmation password do not match.";

    @Test
    public void incorrectRegisterWithPassConfirm(){
        new LoginPage(driver)
                .clickRegisterLnk()
                    .assertPageHeaderIsShown()
                .typeEmail(email)
                .typePassword(pass)
                .typeConfirmPass(confirmPass)
                .clickRegisterWithFailure()
                    .assertPassConfirmErrorIsShown(expPassError);
    }
}
