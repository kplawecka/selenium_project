import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.LoginPage;

public class Incorrect_Register_Invalid_Pass_Test extends SeleniumBaseTest {

    private String email = faker.internet().emailAddress();

    @DataProvider
    public static Object[][] invalidPasswords(){
        return new Object[][]{
                {"test", "The Password must be at least 6 and at max 100 characters long."},
                {"test123.", "Passwords must have at least one uppercase ('A'-'Z')."},
                {"Test123", "Passwords must have at least one non alphanumeric character."},
                {"Tester.", "Passwords must have at least one digit ('0'-'9')."}
        };
    }

    @Test(dataProvider = "invalidPasswords")
    public void incorrectRegisterWithPassValidation(String password, String expError){
        new LoginPage(driver)
                .clickRegisterLnk()
                    .assertPageHeaderIsShown()
                .typeEmail(email)
                .typePassword(password)
                .typeConfirmPass(password)
                .clickRegisterWithFailure()
                    .assertRegistrationError(expError);
    }
}
