import org.testng.annotations.Test;
import pages.LoginPage;

public class Menu_Test extends SeleniumBaseTest {

    private String email = config.getApplicationUser();
    private String password = config.getApplicationPassword();

    @Test
    public void menuTest(){
        new LoginPage(driver)
                .typeEmail(email)
                .typePassword(password)
                .submitLogin()
                .goToProcesses()
                    .assertProcessesIsShown()
                    .assertProcessessUrl()
                .goToCharacteristics()
                    .assertCharacteristicsIsShown()
                    .assertCharacteristicsUrl()
                .goToDashboard()
                    .assertDashboardIsShown()
                    .assertDashboardUrl();
    }
}
