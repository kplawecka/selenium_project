import com.github.javafaker.Faker;
import config.Config;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import java.util.concurrent.TimeUnit;

public class SeleniumBaseTest {

    protected WebDriver driver;
    protected Config config = new Config();
    protected Faker faker = new Faker();

    @BeforeSuite
    public void baseBeforeSuite(){
        System.setProperty("webdriver.chrome.driver", "C:/dev/driver/chromedriver_win32/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }

    @BeforeMethod
    public void baseBeforeMethod(){
        driver.manage().deleteAllCookies();
        driver.get(config.getApplicationUrl());
    }

    @AfterSuite
    public void baseAfterSuite(){
        driver.quit();
    }
}
